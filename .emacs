; init.el 
;
; Emacs custom initializaiton file
; modified [2013.07.23] by <williamdemeo@gmail.com>
; modified [2011.11.29] by <williamdemeo@gmail.com>
; modified [2009.04.26] by <williamdemeo@gmail.com>
; modified [2004.01.09] by <williamdemeo@yahoo.com>


;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(message "Loading init.el")

(load "~/git/lab/wjd/utils/dotfiles-setup/emacs.d/lisp/custom.el")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["black" "#d55e00" "#009e73" "#f8ec59" "#0072b2" "#cc79a7" "#56b4e9" "white"])
 '(case-replace nil)
 '(column-number-mode t)
 '(text-scale-mode-step 1.05)
 '(fill-column 80)
 '(font-lock-maximum-decoration t)
 '(fringe-mode 1 nil (fringe))
 '(gutter-buffers-tab-visible-p nil)
 '(org-journal-dir "~/git/lab/williamdemeo/org-projects/journal")
 '(package-selected-packages
   (quote
    (latex-pretty-symbols zenburn-theme zenburn unicode-math-input unicode-input unicode-fonts unicode-escape org-journal markdown-mode magit latex-unicode-math-mode intero heroku-theme hc-zenburn-theme flatland-theme company-math)))
 '(paren-mode (quote paren) nil (paren))
 '(scrollbars-visible-p nil)
 '(tool-bar-mode nil)
 '(toolbar-visible-p nil)
 '(unshifted-motion-keys-deselect-region nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;;; ---Agda customizations----------------
(setq load-path (cons "~/git/lab/wjd/UF-Agda_wjd/agda/.cabal-sandbox/bin/" load-path))
(set-fontset-font "fontset-default" nil
                  (font-spec :name "DejaVu Sans"))

(setenv "LD_LIBRARY_PATH"
  (let ((current (getenv "LD_LIBRARY_PATH"))
        (new "/usr/local/lib:~/git/lab/wjd/UF-Agda_wjd/agda/.cabal-sandbox/bin"))
    (if current (concat new ":" current) new)))

(load-file (let ((coding-system-for-read 'utf-8))
                (shell-command-to-string "agda-mode locate")))

(setq auto-mode-alist
   (append
     '(("\\.agda\\'" . agda2-mode)
       ("\\.lagda.md\\'" . agda2-mode))
     auto-mode-alist))

;; '(agda2-include-dirs (quote ("." "/home/williamdemeo/git/PROGRAMMING/AGDA/agda-stdlib/src")))

'(agda2-highlight-coinductive-constructor-face ((t (:foreground "#aaffcc"))))
'(agda2-highlight-datatype-face ((t (:foreground "light blue"))))
'(agda2-highlight-field-face ((t (:foreground "#ff99cc"))))
'(agda2-highlight-function-face ((t (:foreground "#66ccff"))))
'(agda2-highlight-inductive-constructor-face ((t (:foreground "#ccffaa"))))
'(agda2-highlight-keyword-face ((t (:foreground "#ffaa00"))))
'(agda2-highlight-module-face ((t (:foreground "#ffaaff"))))
'(agda2-highlight-number-face ((t (:foreground "light green"))))
'(agda2-highlight-postulate-face ((t (:foreground "#ff7766"))))
'(agda2-highlight-primitive-face ((t (:foreground "#66ccff"))))
'(agda2-highlight-primitive-type-face ((t (:foreground "light blue"))))
'(agda2-highlight-record-face ((t (:foreground "light blue"))))
'(agda2-highlight-string-face ((t (:foreground "#aaffff"))))
