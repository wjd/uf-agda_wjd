-----------------------------------------------------------------------------
-- Metric spaces, graphs and ordered structures
-- -------------------------------------------
--\begin{code}\end{code}

--"*Exercise*. The above equivalence is characterized by induction on identifications as the function that maps the reflexive
-- identification to the identity equivalence. We have the following particular cases of interest:
--  1. *Metric spaces*. If `R` is a type of real numbers, then the axioms can be taken to be those for metric spaces, in which
--      case `M` amounts to the type of metric spaces. Then the above characterizes metric space identification as isometry.
--  2. *Graphs*. If `R` is the type of truth values, and the `axioms` function is constant with value *true*, then `M` amounts
--     to the type of directed graphs, and the above characterizes graph identification as graph isomorphism. We get undirected
--     graphs by requiring the relation to be symmetric in the axioms.
--  3. *Partially ordered sets*. Again with `R` taken to be the type of truth values and suitable axioms, we get posets and other
--     ordered structures, and the above says that their identifications amount to order isomorphisms.

-------------------------------------------------------------------------
-- Topological spaces
-- -------------------
--"We get a type of topological spaces when `R` is the type of truth values and the axioms are appropriately chosen.
--\begin{code}\end{code}

--"When `R` is the type of truth values, the type `(X → R)` is the powerset of `X`, and membership amounts to function
-- application:
-- \begin{code}\end{code}

--"If `(X , 𝓞X , a)` and `(Y , 𝓞Y , b)` are spaces, a homeomorphism can be described as a bijection `f : X → Y` such that
-- the open sets of `Y` are precisely those whose inverse images are open in `X`, which can be written as
-- `(λ (V : ℙ Y) → inverse-image f V ∊ 𝓞X) ≡ 𝓞Y`  Then `ι` defined below expresses the fact that a given bijection is a
-- homeomorphism:
--\begin{code}\end{code}

--"What `ρ` says is that identity function is a homeomorphism, trivially:
--\begin{code}\end{code}

--"Then `θ` amounts to the fact that two topologies on the same set must be the same if they make the identity function into
-- a homeomorphism.
-- \begin{code}\end{code}

--"We introduce notation for the type of homeomorphisms:
--\begin{code}\end{code}

--"*Exercise*. The above equivalence is characterized by induction on identifications as the function that maps the reflexive
-- identification to the identity equivalence.

--"But of course there are other choices for `R` that also make sense. For example, we can take `R` to be a type of real
-- numbers, with the axioms for `X` and `F : (X → R) → R` saying that `F` is a linear functional. Then the above gives a
-- characterization of the identity type of types equipped with linear functionals, in which case we may prefer to rephrase
-- the above as
--\begin{code}\end{code}

--"Linear functions on certain spaces correspond to special kinds of measures by the Riesz representation theorem, and hence
-- in this case the `Space` becomes a type of such kind of measure spaces by an appropriate choice of axioms."

----------------------------------------------
-- Selection spaces
-- ----------------
--\begin{code}\end{code}

--"*Exercise*. The above equivalence is characterized by induction on identifications as the function that maps the reflexive
-- identification to the identity equivalence."

-------------------------------------------------------
-- A contrived example
-- -------------------
--"Here is an example where we need to refer to the inverse of the equivalence under consideration. We take the opportunity
-- to illustrate how the above boiler-plate code can be avoided by defining `sns-data` on the fly, at the expense of readability:
--\begin{code}\end{code}

---------------------------------------------------------
-- Functor algebras
-- --------------------
--"In the following, we don't need to know that the functor preserves composition or to give coherence data for the
-- identification `𝓕-id`.
--\begin{code}\end{code}

--"*Exercise*. The above equivalence is characterized by induction on identifications as the function that maps the reflexive
-- identification to the identity equivalence."

---------------------------------------------------------------
-- Type-valued preorders
-- ----------------------
--"This example is harder than the previous ones. A type-valued preorder on a type `X` is a type-valued relation which is
-- reflexive and transitive. A type-valued, as opposed to asubsingleton-valued, preorder could also be called an ∞-preorder.
--\begin{code}\end{code}

--"A category, also known as a `1`-category, is a type-valued preorder subject to suitable axioms, where the relation `_≤_` is
-- traditionally written `hom`, and where identities are given by the reflexivity law, and composition is given by the transitivity law.
-- We choose to use categorical notation and terminology for type-valued preorders.
-- \begin{code}\end{code}

--"But we will use the shorter notation `Σ S` in this submodule. The type of objects of a type-valued preorder:
-- \begin{code}\end{code}

--"Its hom-types (or preorder):
--\begin{code}\end{code}

--"Its identities (or reflexivities):
--\begin{code}\end{code}

--"Its composition law (or transitivity):
--\begin{code}\end{code}

--"Notice that we have the so-called *diagramatic order* for composition.

--"The functoriality of a pair `F , 𝓕` (where in category theory the latter is also written `F`, by an abuse of notation)
-- says that `𝓕` preserves identities and composition:
--\begin{code}\end{code}

--"In order to express the preservation of identities and composition in traditional form, we first define, locally, symbols for
-- composition in applicative order, making the objects implicit:
--\begin{code}\end{code}

--"And we also make implicit the object parameters of the action of the functor on arrows:
--\begin{code}\end{code}

--"Preservation of identities:
--\begin{code}\end{code}

--"Preservation of composition:
--\begin{code}\end{code}

--"This time we will need two steps to characterize equality of type-valued preorders. The first one is as above, by considering
-- a standard notion of structure:
--\begin{code}\end{code}

--"The above constructions are short thanks to computations-under-the-hood performed by Agda, and may require some effort
-- to unravel. The above automatically gives a characterization of equality of preorders. But this characterization uses another
-- equality, of hom types. The second step translates this equality into an equivalence:
--\begin{code}\end{code}

--"Here Agda silently performs a laborious computation to accept the definition of item `v`:
--\begin{code}\end{code}

--"Combining the two steps, we get the following characterization of equality of type-valued preorders in terms of equivalences:
--\begin{code}\end{code}

--"*Exercise*. The above equivalence is characterized by induction on identifications as the function that maps the reflexive
-- identification to the identity functor."

--"Now we consider type-valued preorders subject to arbitrary axioms. The only reason we need to consider this explicitly is
-- that again we need to combine two steps. The second step is the same, but the first step is modified to add axioms.
--\begin{code}\end{code}

--"Recall that `[_]` is the map that forgets the axioms.
--\begin{code}\end{code}

------------------------------------------------
-- Categories
-- -----------
--"By choosing suitable axioms for type-valued preorders, we get categories:
--\begin{code}\end{code}

--"The axioms say that
--  1. the homs form sets, rather than arbitrary types,
--  2. the identity is a left and right neutral element of composition,
--  3. composition is associative.
--\begin{code}\end{code}

--"The first axiom is subsingleton valued because the property of being a set is a subsingleton type. The second and the third
-- axioms are subsingleton valued in the presence of the first axiom, because equations between elements of sets are
-- subsingletons, by definition of set. And because subsingletons are closed under products, the category axioms form a
-- subsingleton type:
--\begin{code}\end{code}

--"We are now ready to define the type of categories, as a subtype of that of type-valued preorders:
--\begin{code}\end{code}

--"We reuse of above names in a slightly different way, taking into account that now we have axioms, which we simply ignore:
--\begin{code}\end{code}

--"*Exercise*. For type-valued preorders, `functorial 𝓧 𝓐 F 𝓕` is not in  general a subsingleton, but for categories,
-- `is-functorial 𝓧 𝓐 F 𝓕` is always a subsingleton.

--"We now apply the module `type-valued-preorder-with-axioms-identity` to get the following characterization of identity of
-- categories:
--\begin{code}\end{code}

--"The HoTT book has a characterization of identity of categories as equivalence of categories in the traditional sense of
-- category theory, assuming that the categories are univalent in a certain sense. We have chosen not to include the univalence
-- requirement in our notion of category, although it may be argued that *univalent category* is the correct notion of category
-- for univalent mathematics (because a univalent category may be equivalently defined as a category object in a 1-groupoid).
-- In any case, the characterization of equality given here is not affected by the univalence requirement, or any subsingleton-
-- valued property of categories."
