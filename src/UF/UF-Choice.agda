---------------------------------------------------------------------------------------
-- We now come to the section on Choice in UF.
-- We'll skip that section for now and put the code in a separate Choice.agda file.
---------------------------------------------------------------------------------------

--FILE: Choice.agda
--DATE: 16 Apr 2020
--BLAME: <williamdemeo@gmail.com>
--REF: Based on Martin Escardo's course notes
--SEE: https://www.cs.bham.ac.uk/~mhe/HoTT-UF-in-Agda-Lecture-Notes/


--------------------------------------------------------------------------------------------------------
-- Choice in univalent mathematics
{-"We discuss unique choice, univalent choice and global choice.

  1. A simple form of unique choice just holds in our spartan MLTT.

  2. The full form of unique choice is logically equivalent to function extensionality.

  3. Univalent choice implies excluded middle and is not provable or disprovable, but is consistent with univalence.

  4. Global choice contradicts univalence, because it is not possible to choose an element of every inhabited type in way that is invariant under automorphisms.
-}

--------------------------------------------
-- The principle of unique choice.

--"The principle of *simple unique choice* says that if for every `x : X` there is a unique `a : A x` with `R x a`, then
-- there is a specified function `f : (x : X) → A x` such that `R x (f x)` for all `x : X`.
-- This just holds and is trivial, given by projection:
--\begin{code}\end{code}

--"Below we also consider a variation of simple unique choice that works with `∃` (truncated `Σ`) rather than `∃!`.

--"A full form of unique choice is Voevodsky's formulation `vvfunext` of function extensionality, which says that products of singletons are
-- singletons. We show that this is equivalent to our official formulation of unique choice:
--\begin{code}\end{code}

--"This can be read as saying that every single-valued relation is the graph of a unique function.
--\begin{code}\end{code}

--"Here is an alternative proof that derives `hfunext` instead:
--\begin{code}\end{code}

--"The above is not quite the converse of the previous, as there is a universe mismatch, but we do get a logical equivalence by taking `𝓦` to be `𝓥`:
--\begin{code}\end{code}

--"We now give a different derivation of unique choice from function extensionality, in order to illustrate transport along the inverse of `happly`.
-- For simplicity, we assume global function extensionality in the next few constructions.
--\begin{code}\end{code}

--"Simple unique choice can be reformulated as follows using `∃` rather than `∃!`. The statement `is-subsingleton (Σ a ꞉ A x , R x a)`
-- can be read as 'there is at most one `a : A x` with `R x a`.' So the hypothesis of the following is that there is at most one such `a` and at least one
-- such `a`, which amounts to saying that there is a unique such `a`, and hence `simple-unique-choice'` amounts to the same thing as
-- `simple-unique-choice`. However, `simple-unique-choice` can be formulated and proved in our spartan MLTT, whereas `simple-unique-choice'`
-- requires the assumption of the existence of subsingleton truncations so that `∃` is available for its formulation.
--\begin{code}\end{code}

--"In the next few subsections we continue working within the submodule `choice`, in order to have the existence of propositional truncations available,
-- so that we can use the existential quantifier `∃`.

---------------------------------
-- The univalent axiom of choice.
{-"The axiom of choice in univalent mathematics says that 'if for every `x : X` there exists `a : A x` with `R x a`, where `R` is some given relation,
    then there exists a choice function `f : (x : X) → A x` with `R x (f x)` for all `x : X`, provided
      1. `X` is a set,
      2. `A` is a family of sets,
      3. the relation `R` is subsingleton valued.

   This is not provable or disprovable in spartan univalent type theory, but, with this proviso, it does hold in Voevodsky's simplicial model
   (see: https://arxiv.org/abs/1211.2851) of our univalent type theory, and hence is consistent. It is important that we have the condition
   that `A` is a set-indexed family of sets and that the relation `R` is subsingleton valued. For arbitrary higher groupoids, it is not in
   general possible to perform the choice functorially." -}

-- \begin{code}\end{code}

--"We define the axiom of choice in the universe `𝓤` to be the above with `𝓣 = 𝓤`, for all possible `X` and `A` (and `R`).
--\begin{code}\end{code}

-----------------------------------------------
-- A second formulation of univalent choice.

{-"The above is equivalent to another familiar formulation of choice, namely that a set-indexed product of non-empty sets is non-empty, where in a
   constructive setting we strengthen `non-empty` to `inhabited` (but this strengthening is immaterial, because choice implies excluded middle, and
   excluded middle implies that non-emptiness and inhabitation are the same notion)." -}

--\begin{code}\end{code}

--"These two forms of choice are logically equivalent (and hence equivalent, as both are subsingletons assuming function extensionality):
--\begin{code}\end{code}

------------------------------------------------
-- A third formulation of univalent choice.

--\begin{code}\end{code}

--"Notice that we use the hypothesis twice to get the conclusion in the following:
--\begin{code}\end{code}

--"*Exercise*. A fourth formulation of the axiom of choice is that every  surjection of sets has an unspecified section.

--------------------------------------------------
-- Univalent choice gives excluded middle.
--"We apply the third formulation to show that choice implies excluded middle. We begin with the following lemma.
--\begin{code}\end{code}

--"The first consequence of this lemma is that choice implies that every set has decidable equality.
--\begin{code}\end{code}

--"Applying the above to the object `Ω 𝓤` of truth-values in the universe `𝓤`, we get excluded middle:
--\begin{code}\end{code}

--"For more information with Agda code, see https://www.cs.bham.ac.uk/~mhe/agda-new/UF-Choice.html "

-----------------------------
-- Global choice.
--"The following says that, for any given `X`, we can either choose a point of `X` or tell that `X` is empty:
--\begin{code}\end{code}

--"And the following says that we can pick a point of every inhabited type:
--\begin{code}\end{code}

--"We first show that these two forms of global choice are logically equivalent, where one direction requires propositional extensionality
-- (in addition to function extensionality, which is an assumption for this local module).
--\begin{code}\end{code}

--"Two forms of globally global choice:
--\begin{code}\end{code}

--"Which are equivalent, where one direction uses propositional extensionality:
--\begin{code}\end{code}

--"And which are inconsistent with univalence:
--\begin{code}\end{code}

--"See also Theorem 3.2.2 and Corollary 3.2.7 of the HoTT book for a different argument that works with a single, arbitrary universe.
--\begin{code}\end{code}


