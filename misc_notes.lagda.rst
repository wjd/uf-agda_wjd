.. FILE: misc_notes.lagda.rst
.. AUTHOR: William DeMeo
.. DATE: 25 Dec 2019
.. UPDATED: 17 Mar 21..20


.. _miscellaneous notes:

Miscellaneous Notes
====================

.. _universes and levels:

Universes and levels
----------------------

::

   {-# OPTIONS --without-K --exact-split --safe #-}

   module Universes where

    open import Agda.Primitive public
     renaming (
 	       Level to Universe -- We speak of universes rather than of levels.
 	     ; lzero to 𝓤₀       -- Our first universe is called 𝓤₀
 	     ; lsuc to _⁺        -- The universe after 𝓤 is 𝓤 ⁺
 	     ; Setω to 𝓤ω        -- There is a universe 𝓤ω strictly above 𝓤₀, 𝓤₁, ⋯ , 𝓤ₙ, ⋯
 	     )
     using    (_⊔_)               -- Least upper bound of two universes, e.g. 𝓤₀ ⊔ 𝓤₁ is 𝓤₁

   Type = λ ℓ → Set ℓ

   _̇   : (𝓤 : Universe) → Type (𝓤 ⁺)
   𝓤 ̇  = Type 𝓤

   𝓤₁ = 𝓤₀ ⁺
   𝓤₂ = 𝓤₁ ⁺
   𝓤₃ = 𝓤₂ ⁺

   _⁺⁺ : Universe → Universe
   𝓤 ⁺⁺ = 𝓤 ⁺ ⁺

   universe-of : {𝓤 : Universe} (X : 𝓤 ̇ ) → Universe
   universe-of {𝓤} X = 𝓤


The ``̇`` notation maps ``𝓤`` (a universe, i.e., level) to ``Type (𝓤 ⁺)`` (i.e., ``Set (lsuc 𝓤)``).

``Set lzero`` is ``Set 𝓤₀`` (which corresponds to ``Sort 0`` in Lean).

``Set (lsuc lzero)`` is ``Set 𝓤₀ ⁺  = 𝓤₀ ̇`` (which corresponds to `Sort 1 = Type = Type 0` in Lean).

So, ``lzero ̇`` corresponds to ``Set (lsuc lzero)`` (which corresponds to ``Type` in Lean).

Translating standard Agda syntax into MHE notation and Lean syntax:

+----------------------------+---------------------+--------------------------+
|   Agda                     | MHE Notn.           | Lean analog              |
+----------------------------+---------------------+--------------------------+
| lzero                      | 𝓤₀                 | 0 : universe             |
| Set lzero                  | Type 𝓤₀            | Sort 0                   |
| lzero ̇ = Set (lsuc lzero) | 𝓤₀ ̇ = Type 𝓤₀ ⁺   | Sort 1 = Type = Type 0   |
+----------------------------+---------------------+--------------------------+


(Following MHE) we refer to universes by letters ``𝓤``, ``𝓥``, ``𝓦``, ``𝓣`` (type these with, resp, ``\MCU``, ``\MCV``, etc)"

::

  variable
   𝓤 𝓥 𝓦 𝓣 : Universe

-----------------------------------

.. _axiomk:

Axiom K
--------

`nlab <https://ncatlab.org/nlab/show/axiom+K+%28type+theory%29>`_ describes **axiom K** as follows [1]_ :

  "[when added, axiom K turns `intensional type theory <https://ncatlab.org/nlab/show/intensional+type+theory>`_ ] into `extensional type theory <https://ncatlab.org/nlab/show/extensional+type+theory>`_ ---or more precisely, what is called `here <https://ncatlab.org/nlab/show/extensional+type+theory>`_ 'propositionally extensional type theory.' In the language of `homotopy type theory <https://ncatlab.org/nlab/show/homotopy+type+theory>`_ , this means that all types are `h-sets <https://ncatlab.org/nlab/show/h-sets>`_ , accordingly axiom K is incompatible with the `univalence axiom <https://ncatlab.org/nlab/show/univalence+axiom>`_ .

  "Heuristically, the axiom asserts that each `term <https://ncatlab.org/nlab/show/term>`_ of each `identity type <https://ncatlab.org/nlab/show/identity+type>`_ ``Idₐ(x,x)`` (of equivalences of a term ``x`` of type ``a``) is `propositionally equal <https://ncatlab.org/nlab/show/propositional+equality>`_ to the canonical `reflexivity <https://ncatlab.org/nlab/show/reflexive+relation>`_ equality proof ``reflₓ : Idₐ(x, x)``.

  "See also `extensional type theory -- Propositional extensionality <https://ncatlab.org/nlab/show/extensional+type+theory#PropositionalExtensionality>`_ ."

--------------------------------------------------------------------------------

Refutation of UIP
---------------------

The word **set** is reserved (in univalent mathematics) for a type with the following property: for all elements `x, y` of the type, the identity type `x ≡ y` has at most one inhabitant.

This is explained in Escardo's notes [add reference] as follows:

  "Whereas we can make the intuition that `x ≡ x` has precisely one element good by *postulating* a certain `K` axiom due to Thomas Streicher, which comes with Agda by default but we have disabled above, we cannot *prove* that `refl x` is the only element of `x ≡ x` for an arbitrary type `X`. This non-provability result was established by :ref:`Hofmann and Streicher <https://ieeexplore.ieee.org/document/316071>` by giving a model of type theory in which types are interpreted as `1`-groupoids.

  "However, for the elements of *some* types, such as the type `ℕ` of natural numbers, it is possible to prove that any identity type `x ≡ y` has at most one element. Such types are called **sets** in univalent mathematics.

  "If instead of the axiom `K` we adopt Voevodsky's **univalence** axiom, we get specific examples of objects `x` and `y` such that the type `x ≡ y` has multiple elements, *within* the type theory.  It follows that the identity type `x ≡ y` is fairly under-specified in general, in that we can't prove or disprove that it has at most one element.

  "There are two opposing ways to resolve the ambiguity or under-specification of the identity types: (1) We can consider the `K` axiom, which postulates that all types are sets, or (2) we can consider the univalence axiom, arriving at univalent mathematics, which gives rise to types that are more general than sets, the `n`-groupoids and `∞`-groupoids.In fact, the univalence axiom will say, in particular, that for some types `X` and elements `x y : X`, the identity type `x ≡ y` does have more than one element.

  "A possible way to understand the element `refl x` of the type `x ≡ x` is as the "generic identification between the point `x` and itself, but which is by no means necessarily the *only* identitification in univalent foundations. It is generic in the sense that to explain what happens with all identifications `p : x ≡ y` between any two points `x` and `y` of a type `X`, it suffices to explain what happens with the identification `refl x : x ≡ x` for all points `x : X`. This is what the induction principle for identity given by Martin-Löf says, which he called J (we could have called it `≡-induction`, but we prefer to honour MLTT tradition):

  ::
    𝕁 : (X : 𝓤 ̇) (A : (x y : X) -> x ≡ y -> 𝓥 ̇)
     ->  ((x : X) -> A x x (refl x))
     ->  (x y : X) (p : x ≡ y) -> A x y p
    𝕁 X A f x x (refl x) = f x

  "This is related to the :ref:`Yoneda Lemma <https://www.cs.bham.ac.uk/~mhe/yoneda/yoneda.html>` in category theory, for readers familiar with the subject, which says that certain natural transformations are *uniquely determined* by their *action on the identity arrows*, even if they are *defined for all arrows*. Similarly here, `𝕁` is uniquely determined by its action on reflexive identifications, but is *defined for all identifications between any two points*, not just reflexivities.

  "In summary, Martin-Löf's identity type is given by the data

    * `Id`,
    * `refl`,
    * `𝕁`,
    * the above equation for `𝕁`.

  "However, we will not always use this induction principle, because we can instead work with the instances we need by pattern matching on `refl` (which is just what we did to define the principle itself) and there is a :ref:`theorem by Jesper Cockx <https://dl.acm.org/citation.cfm?id=2628139>` that says that with the Agda option `without-K`, pattern matching on `refl` can define/prove precisely what `𝕁` can."


---------------------------------------------------------------------------------------

Some finer and/or subtler points of the theory
-----------------------------------------------

.. Universe.agda Basic.agda Product.agda Identity.agda Transport.agda Negation.agda Peano.agda Singleton.agda
.. Monoid.agda Equality.agda

Most of the notes in this subsection are excerpts from Martin Escardo's illuminating observsations in his notes on HoTT/UF in Agda.  (In the following, and possibly elsewhere, we have use the initials MHE when attributing something to Martin H. Escardo.)

On Martin-Löf's identity type
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

MHE [2]_ presents the identity type `Id` as the "central type constructor of MLTT from the point of view of univalent mathematics." In Agda we can define  as follows:"

::
   data Id {𝓤} (X : 𝓤 ̇) : X -> X -> 𝓤 ̇ where
   refl : (x : X) -> Id X x x

He goes on to explain that, "intuitively, the above definition says the only element of the type `Id X x x` is something called `refl x` (for reflexivity). But, as we shall see in a moment, this intuition turns out to be incorrect."

  "Notice a crucial difference with the previous definitions using `data` or induction: In the previous cases, we defined *types*, namely `𝟘`, `𝟙`, `ℕ`, or a *type depending on type parameters*, namely `_+_`, with `𝓤` and `𝓥` fixed, `_+_ : 𝓤 ̇ → 𝓥 ̇ → 𝓤 ⊔ 𝓥 ̇`.  But here we define a *type family* indexed by the *elements* of a given type, rather than a new type from old types."

Relating Martin-Löf's 𝕁 to the Yoneda Lemma
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

MHE [3]_ , "the **Yoneda Lemma** (see: https://www.cs.bham.ac.uk/~mhe/yoneda/yoneda.html) in category theory...says that certain natural transformations are *uniquely determined* by their *action on the identity arrows*, even if they are *defined for all arrows*."

  MHE relates this to Martin-Löf's ≡-induction principle, 𝕁, which "is uniquely determined by its action on reflexive identifications, but is *defined for all identifications between any two points*, not just reflexivities.

    "In summary, Martin-Löf's identity type is given by the data
      * `Id`
      * `refl`
      * `𝕁`
      * the [≡-induction] equation for `𝕁`"

On excluded middle
~~~~~~~~~~~~~~~~~~~

MHE [4]_ , "the potential failure of excluded middle doesn't say that there may be mysterious subsingletons that fail to be singletons and also fail to be empty. No such things occur in mathematical nature." This assertion is followed in the notes by its formal proof, which is appropriately named `no-unicorns` (see the file `Singletons.agda`).

On carriers that are (not) sets
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

MHE [5]_ , "identifications of monoids are in bijection with monoid isomorphisms, assuming univalence. For this to be the case, it is absolutely necessary that the carrier of a monoid is a *set* rather than an arbitrary type, for otherwise the monoid equations can hold in many possible ways, and we would need to consider a notion of monoid isomorphism that in addition to preserving the neutral element and the multiplication, preserves the associativity identifications."

--------------------------------------------------------------------------------------

Writing Agda definitions interactively
----------------------------------------

Here is a description of some Agda key-bindings and how to use them, as we briefly mentioned in the talk.

1. Add a question mark and then type ``C-c C-l`` to create a new "hole."

2. Type ``C-c C-f`` to move into the next unfilled hole.

3. Type ``C-c C-c`` (from inside the hole) to be prompted for what type should fill the given hole.

4. Type ``t`` (or whatever variable you want to induct on) to split on the variable in the hole.

5. Type ``C-c C-f`` to move into the next hole.

6. Type ``C-c C-,`` to get the type required in the current hole.

7. Enter an appropriate object in the hole and type ``C-c C-space`` to remove the hole.

**SUMMARY**.

* ``?`` then ``C-c C-l`` creates hole
* ``C-c C-f`` moves to next hole
* ``C-c C-c`` prompts for what goes in hole
* ``m`` splits (inducts) on variable ``m``
* ``C-c C-,`` in hole gets type required
* ``C-c C-space`` removes hole


----------------------------------------------------------------------------------

Some very useful agda2-mode commands
-------------------------------------

`C-c C-z` Search definitions in scope; see: https://agda.readthedocs.io/en/v2.6.1/tools/search-about.html#search-about

`C-c C-h` Compute type of helper function and add type signature to kill ring.

`M-m h d c` Information about character at point.

`M-.` Go to definition of identifier under point (same as middle mouse button); `M-,` Go back.


--------------------------------------

.. rubric:: Footnotes

.. [1]
   source: https://ncatlab.org/nlab/show/axiom+K+%28type+theory%29
   accessed: 29 Jan 2020

.. [2]
   source: https://www.cs.bham.ac.uk/~mhe/HoTT-UF-in-Agda-Lecture-Notes/HoTT-UF-Agda.html#identitytype
 accessed: 19 Mar 2020

.. [3]
   source: https://www.cs.bham.ac.uk/~mhe/HoTT-UF-in-Agda-Lecture-Notes/HoTT-UF-Agda.html#identitytype
   accessed: 20 Mar 2020

.. [4]
   source: https://www.cs.bham.ac.uk/~mhe/HoTT-UF-in-Agda-Lecture-Notes/HoTT-UF-Agda.html#em
   accessed: 19 Mar 2020
   
.. [5]
   source: https://www.cs.bham.ac.uk/~mhe/HoTT-UF-in-Agda-Lecture-Notes/HoTT-UF-Agda.html#magmasandmonoids
   accessed: 19 Mar 2020


----------------------

.. include:: hyperlink_references.rst
