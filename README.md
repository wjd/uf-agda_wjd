# UF Agda

My personal notes from the HoTT/UF course by Escardo.

## Escardo's lecture notes on HoTT/UF in Agda

* [Main website: www.cs.bham.ac.uk/~mhe/HoTT-UF-in-Agda-Lecture-Notes/index.html](https://www.cs.bham.ac.uk/~mhe/HoTT-UF-in-Agda-Lecture-Notes/index.html)

* [Github repository: github.com/martinescardo/HoTT-UF-Agda-Lecture-Notes](https://github.com/martinescardo/HoTT-UF-Agda-Lecture-Notes)


## Notes

### Agda Installation on Ubuntu 18.04

Following the excellent instructions by Martin Escardo's we found at

https://github.com/martinescardo/HoTT-UF-Agda-Lecture-Notes/blob/master/INSTALL.md

we get a localized cabal "sandbox" with Agda 2.6.1, or whichever version you wish (see below).

Here I list the steps that I took, which I would recommend to others wanting to use Agda (particularly if they wish to try out our Agda universal algebra library (ualib.org)).

(These steps are based on Martin's instructions, with a few minor improvements.)

1. **Install dependencies**.

   ```
   sudo apt install emacs git ghc cabal-install alex happy
   ```

2. **Create a home for the project/tutorial**.

   **NOTE**. To customize all of the commands that follow for your own machine/environment, it probably suffices to simply modify the definition of the global variable UF_HOME in the first line below.

   ```
   export UF_HOME="$HOME/git/lab/wjd/UF-Agda_wjd"
   mkdir $UF_HOME
   ```

3. **Install Agda sandbox from sources in git repository**.

   This step consists of the following substeps:

   * clone agda source code and cd into the cloned directory
   * checkout the desired Agda version,
   * make a cabal sandbox,
   * install Agda in the sandbox.

   ```
   cd $UF_HOME
   git clone https://github.com/agda/agda
   cd agda
   git checkout release-2.6.1  # (but see "Optional" step below)
   cabal sandbox init
   cabal update
   cabal install
   ```

   Now make yourself a beverage (i.e., the last command will take some nontrivial amount of time).

   *Optional*. To see all available Agda releases, type the parital command `git checkout release-` and hit the `Tab` key three times.

4. **Set up Emacs to work with Agda**

   ```
   cd $UF_HOME/agda/.cabal-sandbox/bin/
   touch ~/.emacs
   cp ~/.emacs ~/.emacs.backup
   ./agda-mode setup
   ./agda-mode compile
   cp ~/.emacs $UF_HOME/
   cp ~/.emacs.backup ~/.emacs
   cd $UF_HOME
   echo "#!/bin/bash" > agdamacs
   echo "PATH=$UF_HOME/agda/.cabal-sandbox/bin/:$PATH" >> agdamacs
   echo "emacs --no-init-file --load $UF_HOME/.emacs \$@" >> agdamacs
   chmod +x agdamacs
   ```

   Now to start Emacs with Agda mode, enter the following on the command line:

   ```
   $UF_HOME/agdamacs
   ```

5. **Make startup script runnable from anywhere** (optional)

   You may wish to make a symbolic link in your personal `bin` directory so you can simply type `agdamacs` at the command line (from any directory) to launch the configuration of emacs we have just setup.

```
mkdir -p $HOME/bin
ln -s $UF_HOME/agdamacs $HOME/bin/agdamacs
```

------------------------------------------------

### File dependency and order

The files in this repository are the result of working through the sections of Martin Escardo's brilliantly illuminating set of notes on HoTT/UF in Agda, which Escsardo prepared for the students of his short course at the Midlands Graduate School (MGS) in Computing Science held in 2019 at the University of Birmingham in England.

Martin's notes consist of a single literate Agda file called HoTT-UF-Agda.lagda which can be used to generate a single Agda file, HoTT-UF-Agda.agda, which can then be type-checked by Agda.

There are obvious reasons for wanting to have everything in a single file for teaching a course.  However, when working through the notes I decided to split the topics into separate Agda files because I believe this will make it easier to adapt parts of the development to our own Agda projects.  Also, doing so gives practice with Agda's module and import mechanisms.

#### Files created while working through Martin's notes

**Chapter 2.**

  Basic.agda   

**Chapter 3.**

  Singleton.agda   
  Equality.agda  
  Univalence.agda  
  Extensionality.agda  
  Monoid.agda  
  Embedding.agda  
  Structures.agda  
  Truncation.agda  
  MetricSpaces.agda

----------------------------------------------------------------------

### Special notation used in the Agda code.

The ``̇`` operator maps a universe ``𝓤`` (i.e., a level) to ``Set 𝓤``, and the latter has type ``Set (lsuc 𝓤)``, a.k.a. ``Type (𝓤 ⁺)``.
That is,  ``𝓤 ̇``  is simply an alias for  ``Set 𝓤.``

The level ``lzero`` is renamed  ``𝓤₀``, so ``𝓤₀ ̇`` is an alias for ``Set lzero``.  (This corresponds to ``Sort 0`` in Lean.)

Thus,   ``Set (lsuc lzero)``    is    ``Set 𝓤₀ ⁺``    which is denoted by  ``𝓤₀ ⁺ ̇``.

#### Table: translation from standard Agda syntax into MHE notation and Lean syntax

| Agda | MHE Notation | Lean analog |
| --- | --- | --- |
|  `Level`  |   `Universe`  |  `universe`  |
|   `lzero`   |   `𝓤₀`  |  `0 : universe` |
|  `Set lzero` |   `𝓤₀ ̇` ( = `Type 𝓤₀`)  |  `Sort 0`     |
| `lsuc lzero` |   `𝓤₀ ⁺` |  `1 : universe`   |
|  `Set (lsuc lzero)` |   `𝓤₀ ⁺ ̇` | `Sort 1 = Type = Type 0` |

---------------------------------------------

### Unicode input

Reference: https://agda.readthedocs.io/en/v2.6.1/tools/emacs-mode.html

In general, to find out the available keybindings for symbols in agda-mode, do `M-x describe-input-method [Return] Agda`.

More precisely, to get information about a specific character in the buffer, move the point to that character and type `C-u C-x =`.

Here are some of the special cases that we use often.

| To type | emacs (agda-mode) |
| ------- | ----------------- |
| `𝓤₀ ̇` | \MCU\\_0 \\^.     |
| `𝓤`    | \MCU      |
| `𝟘`    | \b0       |
| `𝟙`    | \b1       |
| `×`    | \times    |
| `∘`    | \circ     |
| `≡`    | \equiv    |
| `≃`    | \~-       |
| `≅`    | \cong     |
| `≈`    | \~~       |
| `⇔`    | \lr2      |
| `∙`    |  \.       |
| `∙`    | \cdot     |
| `●`    | \cib      |
| `⟨`    | \langle   |
| `⟩`    | \rangle   |
| `∎`    | \qed      |
| `■`    | \sq1      |
| `⁻¹`   | \^-\^1    |
| `◁`    | \lhd      |
| `◀`    | \t1       |
| `★`    | \st7      |
| `⋆`    | \* or \star | 
| `∈`    | \in       |
| `⋃`    | \Un       |
| `∨`    | \vee, \or |
| `×`    | \x        |
| `Σ x ꞉ X , A x` | \Sigma x \:4 X , A x |

N.B. the colon (:) symbol that appears in the dependent pair (Σ) type---i.e., `Σ x ꞉ X , A x`---is entered by typing `\:4`.


---------------------------------------------


### Font faces

To install nicer fonts, follow the instructions at

https://gist.github.com/lightonphiri/5811226a1fba0b3df3be73ff2d5b351c

Summary (with some additional/optional steps):

1. download some fonts, e.g., 

   https://fonts.google.com/specimen/Mali
   
   (The following steps assume the fonts are downloaded in a zip file in the `~/opt/FONTS` directory.)

2. unzip them in your local font directory

   ```
   mkdir -p ~/.fonts/googlefonts
   cd !$
   unzip -d . ~/opt/FONTS/*.zip 
   
3. unzip the fonts in a system directory

   ```
   sudo mkdir -p /usr/share/fonts/googlefonts
   cd !$
   sudo unzip -d . ~/opt/FONTS/*.zip
   sudo chmod -R --reference=/usr/share/fonts/opentype /usr/share/fonts/googlefonts

4. make the new fonts available by registering them

   ```
   sudo fc-cache -fv
   ```
   
5. Check if font installed with, e.g., `fc-match Fondamento`

6. Test them out by opening this file in emacs and running the command `M-x set-frame-font`

   upper alpha: ABCDEFGHIJKLMNOPQRSTUVWXYZ
   lower alpha: abcdefghijklmnopqrstuvwxyz
   numbers: 0 1 2 3 4 5 6 7 8 9
   
   compare similar characters: 
   0  O  o ,  1  l  L i , 2  z , 5  S , :  ꞉ 

   other symbols that are important for us: 
 
     -> →  ⇔ ¬A  𝓤  𝓤₀ ̇  𝟘   𝟙   ∘  ≡  ∙ ⟨ ⟩ ∎  f⁻¹ 
     
     ◁ ≃ ● ■  ∈ 

     {A : X -> 𝓤₀ ̇}     Σ x ꞉ X , A x

-------------------------

My current fav setup is a slightly modified zenburn theme with Neucha 18 point font.

I also like "Nanum Gothic Coding", but have two major problems with that font.
First it makes inverses `f⁻¹` look weird, like this: `f⁻ ¹`
Also, the negation symbol is elongated and appears too low.

Here's a list of some other nice font sets:

`Baloo_Thambi_2`
`Comic_Neue`
`Delius_Swash_Caps`
`Gamja_Flower`
`Gochi Hand`
`Kalam`
`Mali`
`Sofia`
`Neucha`
`The_Girl_Next_Door`


### Operator fixities and precedences

Without the following list of operator precedences and associativities (left or right), the agda file HoTT-UF-Agda.agda doesn't parse.

```agda
   infix   0 _∼_
   infixr 50 _,_
   infixr 30 _×_
   infixr 20 _+_
   infixl 20 _∨_
   infixl 70 _∘_
   infix   0 _≡_
   infix  10 _⇔_
   infixl 30 _∙_
   infixr  0 _≡⟨_⟩_
   infix   1 _∎
   infix  40 _⁻¹
   infix  10 _◁_
   infixr  0 _◁⟨_⟩_
   infix   1 _◀
   infix  10 _≃_
   infixl 30 _●_
   infixr  0 _≃⟨_⟩_
   infix   1 _■
   infix  40 _∈_
   infix  30 _[_,_]
   infixr -1 -Σ
   infixr -1 -Π
   infixr -1 -∃!
   infix  20 _∩_
   infix  20 _∪_
```

