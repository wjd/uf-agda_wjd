-- FILE: Bergman.agda
-- BLAME: williamdemeo@gmail.com
-- DATE: 5 Apr 2020
-- DESC: Solutions to Exercises in Universal Algebra mostly from Cliff Bergman's textbook.
-- Eventually we want to code these up in Agda; that's why we're naming the file with an .agda
-- extension and including the file in this repository.

{-

  Show that for any algebra A and a,b∈A, Θ(a,b)=t* ( s ( { ( p(a , c⁻), p(b , c⁻) )  :  p(x, y₁, …, yₙ) is a term, c₁, …, cₙ ∈A } ) ) ⋃ ΔA,
  where t*(  ) is the transitive closure operator, i.e., for Y⊆A×A, t∗(Y) is the smallest subset of A×A containing Y and closed under t.


Exercise Set 1.16.

5. An element a of an algebra A is called a non-generator of A if, for every
   X ⊆ A, A = Sg(X ⋃ {a}) implies A = Sg{X).

   (a) Prove that the set of nongenerators of A forms a subuniverse, Frat(A)
       (called the Frattini subuniverse of A).

   (b) Prove that Frat(A) is the intersection of all maximal proper subuniverses of A.
       (If you wish, you can assume that A is finite. To do the infinite case, you will
       need Zorn’s lemma.)

SOLUTION.
   (b) Let 𝓜 = {Mᵢ : i ∈ I} be the collection of all maximal proper subuniverses of A.

       Let Frat(A) be the collection of non-generators of A.

       To show: ⋂ 𝓜 = Frat(A)

       1. Frat(A) ⊆ ⋂ 𝓜. Let u be a non-generator of A and show that ∀i ∈ I, u ∈ Mᵢ as follows: Fix i.
          If u does not belong to Mᵢ, then Sg(Mᵢ,u) = A. But then Mᵢ = Sg(Mᵢ) = A, since u is a non-generator.
          On the other hand, Mᵢ is proper. This contradiction proves that u belongs to Mᵢ, and i was arbitrary.

       2. ⋂ 𝓜 ⊆ Frat(A). Let u ∈ ⋂ 𝓜. We prove u is a non-generator. Suppose A = Sg (X ∪ {u}) for some
          X ⊆ A. To see that Sg(X) = A, suppose the contrary; i.e., Sg(X) ≠ A. Then (by Zorn's Lemma) there
          exists a maximal proper subuniverse M ≤ A containing Sg(X). But then u ∈ ⋂ 𝓜 ⊆ M implies that
          M contains both X and u and thus M contains A = Sg (X ∪ {u}), contradicting the fact that M is
          proper. ∎




Exercise Set 2.4

3. (a) Let P be a poset. Show that there is no surjective, isotone map from P to (Dn(P), C).
      (Hint: suppose f were such a map. Consider the set B = {x ∈ P : (∃y) x ≤ y & y ∉ f(y)}.)
   (b) Use part (a) to prove Cantor’s theorem: for any set X, there is no surjective function
       from X to Sb(X).

SOLUTION.
   (a) The set B cannot be empty.

       Reason: since f is surjective, something gets mapped to the empty set. If f(x) = ∅, then
       x ∈ B since ∃y (e.g., y = x) with the given properties.

       Observe that B is a downset, so f maps something in P to B. Let b ∈ P be such that f(b) = B.

       If b ∉ B, then there is no y ≥ b such that y ∉ f(y). But what about b? ...we have b ≥ b and
       b ∉ B = f(b), so b satisfies the criteria for membership in B (contradiction).

       Since assuming b ∉ B leads to a contradiction, assume b ∈ B. Then by definition there
       exists y ≥ b such that y ∉ f(y).  Let f(y) = C. (So we have y ∉ C.)

       Now use monotonicity to obtain the contradiction y ∈ C.

-}
